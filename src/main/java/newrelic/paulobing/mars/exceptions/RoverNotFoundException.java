package newrelic.paulobing.mars.exceptions;

public class RoverNotFoundException extends Exception {
    private static final long serialVersionUID = 1L;

    public RoverNotFoundException(String index) {
        super("Rover not found! Index: [" + index + "]");
    }

}
