package newrelic.paulobing.mars.exceptions;

import newrelic.paulobing.mars.model.Coordinates;

public class OutOfPlateauBoundaryException extends Exception {
    private static final long serialVersionUID = 1L;

    public OutOfPlateauBoundaryException(Coordinates newRoverCoordinates) {
        super("Coordinate is outside the plateau boundary. Coordinates [" + newRoverCoordinates.csv() + "]");
    }

}
