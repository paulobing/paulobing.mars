package newrelic.paulobing.mars.model;

public class Plateau {
    private Coordinates lowerLeftBoundary;
    private Coordinates upperRightBoundary;

    public Plateau(int lowerLeftBoundaryX, int lowerLeftBoundaryY, int upperRightBoundaryX, int upperRightBoundaryY) {
        this(new Coordinates(lowerLeftBoundaryX, lowerLeftBoundaryY),
                new Coordinates(upperRightBoundaryX, upperRightBoundaryY));
    }

    public Plateau(Coordinates lowerLeftBoundary, Coordinates upperRightBoundary) {
        this.lowerLeftBoundary = lowerLeftBoundary;
        this.upperRightBoundary = upperRightBoundary;
    }

    public boolean isWithinBoundaries(Coordinates coordinates) {
        return coordinates.isBetween(lowerLeftBoundary, upperRightBoundary);
    }
}
