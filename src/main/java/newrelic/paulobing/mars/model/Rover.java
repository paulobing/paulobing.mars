package newrelic.paulobing.mars.model;

import newrelic.paulobing.mars.enums.CardinalPoint;
import newrelic.paulobing.mars.enums.RoverOperation;

public class Rover {
    private int index;
    private Coordinates coordinates;
    private CardinalPoint roverDirection;

    public Rover(int index, Coordinates coordinates, CardinalPoint roverDirection) {
        this.index = index;
        this.coordinates = coordinates;
        this.roverDirection = roverDirection;
    }

    public int getIndex() {
        return index;
    }
    
    public Coordinates getCoordinates() {
        return coordinates;
    }

    public CardinalPoint getRoverDirection() {
        return roverDirection;
    }

    public Coordinates createNextOperationCoordinates() {
        Coordinates candidateCoordinates = coordinates.clone();
        candidateCoordinates.incrementX(roverDirection.getX());
        candidateCoordinates.incrementY(roverDirection.getY());
        return candidateCoordinates;
    }

    public void faceNewDirection(RoverOperation operation) {
        if (operation == RoverOperation.L) {
            roverDirection = roverDirection.turnLeft();
        } else if (operation == RoverOperation.R) {
            roverDirection = roverDirection.turnRight();
        }
    }

    public Rover applyCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
        return this;
    }
}
