package newrelic.paulobing.mars.model;

public class Coordinates {
    private int x;
    private int y;

    public Coordinates(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Coordinates clone() {
        return new Coordinates(x, y);
    }

    public Coordinates incrementX(int amount) {
        x += amount;
        return this;
    }

    public Coordinates incrementY(int amount) {
        y += amount;
        return this;
    }

    public boolean isBetween(Coordinates lowerLeftBoundary, Coordinates upperRightBoundary) {
        return x >= lowerLeftBoundary.getX() && y >= lowerLeftBoundary.getY() && x <= upperRightBoundary.getX()
                && y <= upperRightBoundary.getY();
    }

    public String csv() {
        return x + "," + y;
    }
}
