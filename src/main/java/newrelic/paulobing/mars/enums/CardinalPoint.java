package newrelic.paulobing.mars.enums;

public enum CardinalPoint {
    N(0, 1), E(1, 0), S(0, -1), W(-1, 0);

    private int x;
    private int y;

    CardinalPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public CardinalPoint turnLeft() {
        switch (this) {
        case N:
            return W;
        case W:
            return S;
        case S:
            return E;
        case E:
            return N;
        default:
            return this;
        }
    }

    public CardinalPoint turnRight() {
        switch (this) {
        case N:
            return E;
        case E:
            return S;
        case S:
            return W;
        case W:
            return N;
        default:
            return this;
        }
    }

}
