package newrelic.paulobing.mars.enums;

public enum RoverOperation {
    L("LEFT"), R("RIGHT"), M("MOVE");

    private String roverOperationName;

    RoverOperation(String roverOperationName) {
        this.roverOperationName = roverOperationName;
    }

    public String getRoverOperationName() {
        return roverOperationName;
    }

    public static RoverOperation valueOf(char roverOperationChar) {
        return valueOf("" + roverOperationChar);
    }

}
