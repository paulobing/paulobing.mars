package newrelic.paulobing.mars.controller;

import java.util.List;
import java.util.Map;

import newrelic.paulobing.mars.enums.RoverOperation;
import newrelic.paulobing.mars.exceptions.OutOfPlateauBoundaryException;
import newrelic.paulobing.mars.exceptions.RoverNotFoundException;
import newrelic.paulobing.mars.model.Plateau;
import newrelic.paulobing.mars.model.Rover;
import newrelic.paulobing.mars.service.RawOutputService;
import newrelic.paulobing.mars.service.RoverService;

public class MarsRoversController {
    private RoverService roverService;
    private RawOutputService rawOutputService;
    private Map<Rover, List<RoverOperation>> mapRoversRoverOperations;
    private Plateau plateau;
    
    public MarsRoversController() {
        roverService = new RoverService();
        rawOutputService = new RawOutputService();
    }
    
    public String buildRawOutput() {
        return rawOutputService.buildRawOutput(mapRoversRoverOperations);
    }

    // example "5 5 1 2 N LMLMLMLMM 3 3 E MMRMMRMRRM"
    public void receiveRawInput(String rawInput) {
        plateau = rawOutputService.buildPlateauFromRawInput(rawInput);
        mapRoversRoverOperations = rawOutputService.buildRoversRoverOperationsFromRawInput(rawInput);
    }

    public void processDataAndMoveRovers() throws OutOfPlateauBoundaryException {
        for(Rover rover: mapRoversRoverOperations.keySet()) {
            List<RoverOperation> roverOperations = mapRoversRoverOperations.get(rover);
            for(RoverOperation roverOperation: roverOperations) {
                roverService.operateRover(plateau, rover, roverOperation);
            }
        }
    }

    public Rover chooseRover(String roverIndex) throws RoverNotFoundException {
        try {
            int roverIndexInt = Integer.parseInt(roverIndex);
            return mapRoversRoverOperations.keySet().stream().filter(rover -> rover.getIndex() == roverIndexInt).findFirst().orElseThrow(() -> new RoverNotFoundException(roverIndex));
        } catch (RuntimeException e) {
            throw new RoverNotFoundException(roverIndex);
        }
    }
    
    // example LMLMLMLMMR
    public void moveRoverWithRawRemoteCommand(Rover rover, String remoteCommandRawOperations) throws OutOfPlateauBoundaryException {
        List<RoverOperation> listRemoteCommandOperations = rawOutputService.buildRemoteCommandRoverOperations(remoteCommandRawOperations);
        for(RoverOperation roverOperation: listRemoteCommandOperations) {
            roverService.operateRover(plateau, rover, roverOperation);
        }
    }
}
