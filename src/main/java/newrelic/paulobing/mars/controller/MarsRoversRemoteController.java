package newrelic.paulobing.mars.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import newrelic.paulobing.mars.exceptions.OutOfPlateauBoundaryException;
import newrelic.paulobing.mars.exceptions.RoverNotFoundException;
import newrelic.paulobing.mars.model.Rover;

/**
 * 
 * <p><b>Server</b>: run as main or execute on command line on project directory:</p>
 *  <p>mvn exec:java -D"exec.mainClass"="newrelic.paulobing.mars.controller.MarsRoversRemoteController"</p>
 * <p><b>Client</b>: The remote interface can be accessed on PuTTY for instance on localhost:8084 using "raw" Connection Type</p>
 *
 */
public class MarsRoversRemoteController {
    private MarsRoversController controller = new MarsRoversController();
    
    public static void main(String[] args) throws IOException {
        MarsRoversRemoteController remoteController = new MarsRoversRemoteController();
        remoteController.run();
    }
    
    public void run() throws IOException {
        try (ServerSocket serverSocket = new ServerSocket(8084)) {
            Socket socket = serverSocket.accept();
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            
            showInstructionsAndProcessFirstRawInput(out, in);
            loopAndProcessRemoteCommandInput(out, in);
        }
    }

    private void loopAndProcessRemoteCommandInput(PrintWriter out, BufferedReader in) throws IOException {
        out.println("Choose rover by index (example: 1). 'exit' to leave");
        String inputLine = in.readLine();
        while (inputLine != null && !inputLine.equals("exit")) {
            try {
                Rover rover = controller.chooseRover(inputLine);
                out.printf("You are using rover [%d]%n", rover.getIndex());
                out.println("Please input operations for this rover:");
                out.println("Example: RRMMLMR");
                
                inputLine = in.readLine();
                controller.moveRoverWithRawRemoteCommand(rover, inputLine);
            } catch(OutOfPlateauBoundaryException e) {
                out.println("Invalid move. " + e.getMessage());
            } catch (RoverNotFoundException e) {
                out.println("Invalid rover. " + e.getMessage());
            } catch (Exception e) {
                out.println("Unexpected error! Please input valid data!");
            }
            out.println(controller.buildRawOutput());
            out.println("Choose rover by index (example: 1). 'exit' to leave");
            inputLine = in.readLine();
        }
    }

    private void showInstructionsAndProcessFirstRawInput(PrintWriter out, BufferedReader in) {
        out.println("INPUT: The first snippet of input is the upper-right coordinates of the plateau, the lower-left " + 
                "coordinates are assumed to be 0,0. The rest of the input is information pertaining to the " + 
                "rovers that have been deployed. Each rover has two snippets of input. The first snippet gives " + 
                "the rover's position, and the second snippet is a series of instructions telling the rover how to " + 
                "explore the plateau. The position is made up of two integers and a letter separated by " + 
                "spaces, corresponding to the x and y coordinates and the rover's orientation. Each rover will " + 
                "be finished sequentially, which means that the second rover won't start to move until the first " + 
                "one has finished moving.\r\n");
        out.println("Example: 5 5 1 2 N LMLMLMLMM 3 3 E MMRMMRMRRM");
        out.println("Please enter raw input and hit 'Enter'\r\n");
        boolean ok = false;
        while (!ok) {
            try {
                String firstRawInput = in.readLine();
                controller.receiveRawInput(firstRawInput);
                try {
                    controller.processDataAndMoveRovers();
                    ok = true;
                    out.println(controller.buildRawOutput());
                } catch(OutOfPlateauBoundaryException e) {
                    out.println("Invalid move. " + e.getMessage());
                }
            } catch (Exception e) {
                out.println("Invalid input! Please follow instructions above. Message: " + e.getMessage());
            }
        }
    }
}
