package newrelic.paulobing.mars.service;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import newrelic.paulobing.mars.enums.CardinalPoint;
import newrelic.paulobing.mars.enums.RoverOperation;
import newrelic.paulobing.mars.model.Coordinates;
import newrelic.paulobing.mars.model.Plateau;
import newrelic.paulobing.mars.model.Rover;

public class RawOutputService {
    public String buildRawOutput(Map<Rover, List<RoverOperation>> mapRoversRoverOperations) {
        String rawOutput = "";
        for (Rover rover : mapRoversRoverOperations.keySet()) {
            rawOutput += String.format("%d %d %s ",
                    rover.getCoordinates().getX(),
                    rover.getCoordinates().getY(),
                    rover.getRoverDirection().name());
        }
        return rawOutput.trim();
    }

    public Plateau buildPlateauFromRawInput(String rawInput) {
        return new Plateau(0, 0, Integer.parseInt(rawInput.substring(0, 1)),
                Integer.parseInt(rawInput.substring(2, 3)));
    }

    public Map<Rover, List<RoverOperation>> buildRoversRoverOperationsFromRawInput(String rawInput) {
        Map<Rover, List<RoverOperation>> mapRoversRoverOperations = new LinkedHashMap<>();
        String roverRawInput = rawInput.substring(4);
        String regex = "([0-9]) ([0-9]) ([NESW]) ([LMR]+)";
        Matcher matcher = Pattern.compile(regex).matcher(roverRawInput);
        int roverIndex = 0;
        while (matcher.find()) {
            Coordinates roverCoordinates = new Coordinates(Integer.parseInt(matcher.group(1)),
                    Integer.parseInt(matcher.group(2)));
            CardinalPoint cardinalPoint = CardinalPoint.valueOf(matcher.group(3));
            String rawRoverOperations = matcher.group(4);

            Rover rover = new Rover(roverIndex++, roverCoordinates, cardinalPoint);
            List<RoverOperation> roverOperations = buildRemoteCommandRoverOperations(rawRoverOperations);
            mapRoversRoverOperations.put(rover, roverOperations);
        }

        return mapRoversRoverOperations;
    }
    
    public List<RoverOperation> buildRemoteCommandRoverOperations(String remoteCommandRawOperations) {
        return remoteCommandRawOperations.chars()
                .mapToObj(r -> RoverOperation.valueOf((char) r))
                .collect(Collectors.toList());
    }
}
