package newrelic.paulobing.mars.service;

import newrelic.paulobing.mars.enums.RoverOperation;
import newrelic.paulobing.mars.exceptions.OutOfPlateauBoundaryException;
import newrelic.paulobing.mars.model.Coordinates;
import newrelic.paulobing.mars.model.Plateau;
import newrelic.paulobing.mars.model.Rover;

public class RoverService {
    public void operateRover(Plateau plateau, Rover rover, RoverOperation operation) throws OutOfPlateauBoundaryException {
        if (operation != RoverOperation.M) {
            rover.faceNewDirection(operation);
        } else {
            Coordinates candidateCoordinates = rover.createNextOperationCoordinates();

            if (plateau.isWithinBoundaries(candidateCoordinates)) {
                rover.applyCoordinates(candidateCoordinates);
            } else {
                throw new OutOfPlateauBoundaryException(candidateCoordinates);
            }
        }
    }

}
