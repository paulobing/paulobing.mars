package newrelic.paulobing.mars.controller;

import static org.junit.Assert.*;

import org.junit.Test;

import newrelic.paulobing.mars.exceptions.OutOfPlateauBoundaryException;

public class MarsRoversTest {
    @Test
    public void testRoversFinalCoordinateAndHeading() throws OutOfPlateauBoundaryException {
        String rawInput = "5 5 1 2 N LMLMLMLMM 3 3 E MMRMMRMRRM";
        MarsRoversController mars = new MarsRoversController();
        mars.receiveRawInput(rawInput);
        mars.processDataAndMoveRovers();
        String rawOutput = mars.buildRawOutput();
        assertEquals("1 3 N 5 1 E", rawOutput);
    }
    @Test(expected=OutOfPlateauBoundaryException.class)
    public void testExceptionWhenMovingOutsidePlateau() throws OutOfPlateauBoundaryException {
        String rawInput = "5 5 1 2 N MMMMMM";
        MarsRoversController mars = new MarsRoversController();
        mars.receiveRawInput(rawInput);
        mars.processDataAndMoveRovers();
    }
}
